#pragma once
#include "Piece.h"
#include "Board.h"
#define BOARD_SIZE 7
class Bishop : public Piece
{
public:
	//Constructor
	Bishop(std::string type, unsigned int loc[], std::string color);

	//Distrcutor
	virtual ~Bishop();



	//Methods
	//bool movePiece(char** slots, unsigned int pos[]);
	virtual int canEat(Piece*** slots, unsigned int pos[]);
};


