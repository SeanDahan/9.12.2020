#include "Rook.h"
//
Rook::Rook(std::string type, unsigned int loc[], std::string color) : Piece(type, loc, color)
{
}

Rook::~Rook()
{
}

/*bool Rook::movePiece(char** slots, unsigned int pos[])
{
	return true;
}*/

int Rook::canEat(Piece*** slots, unsigned int pos[])
{
	int appli = 0, sel = 0;//if the rook moves up, then apply is 1, if it moves down then apply is -1
							//sel separates the type of the move, rows or colum
	unsigned int newLoc[2] = { _location[0], _location[1] };
	std::cout << "First color " << slots[pos[0]][pos[1]]->getColor() << " Second Color " << this->_color << " Position to eat is " << pos[0] << pos[1] << " Current loc is " << this->_location[0] << this->_location[1] << std::endl;
	if (pos[0] >= BOARDSIZE || pos[0] < 0 || pos[1] >= BOARDSIZE || pos[1] < 0)//If move is out of the board
	{
		return FIVE;
	}
	else if (pos[0] == this->_location[0] && this->_location[1] == pos[1]) //If the move is to the same spot we are currently in
	{
		return SEVEN;
	}
	else if (slots[pos[0]][pos[1]]->getColor() == this->getColor() && slots[pos[0]][pos[1]]->getType() != EMPTY) //If there is a piece in the same color as ours in the destination spot
	{
		return THREE;
	}
	else if (pos[0] == this->_location[0] && !(this->_location[1] == pos[1]) || !(pos[0] == this->_location[0]) && this->_location[1] == pos[1]) //checking the rook moves in a stright line, using XOR functionality
	{
		if (pos[0] == this->_location[0]) //if the rook moves in a colum, sel = 1 else sel = 0
		{
			sel = 1;
		}

		if (pos[sel] > this->_location[sel]) //Check if the rook moves up or down (colum), left or right (row)
		{
			appli = 1; //moving up/right
		}
		else
		{
			appli = -1; //moving down/left
		}

		for (size_t i = this->_location[sel]; i != pos[sel]; i+=appli) //loop checks that there is no other piece in the rook's way
		{
			newLoc[sel] += appli; //adding or substructing slots, by the move's type
			if (!slots[newLoc[0]][newLoc[1]]->isEmpty() &&  newLoc[sel] != pos[sel]) //Checking if there is a piece in the current spot. if yes, return false
			{
				return SIX;
			}
		}


		return 0; // zero means that the move is valid
	}
	return SIX;
} //Sean's way
