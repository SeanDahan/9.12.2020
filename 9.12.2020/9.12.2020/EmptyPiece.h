#pragma once
#include "Piece.h"
//
class Board;
class EmptyPiece : public Piece
{
public:
	//Constructor
	EmptyPiece(std::string type, unsigned int loc[2],std::string color);
	EmptyPiece();

	//Distructor
	virtual ~EmptyPiece();

	//Methods
	bool movePiece(char** slots, unsigned int pos[]);
	virtual int canEat(Piece*** slots, unsigned int pos[]);

};
