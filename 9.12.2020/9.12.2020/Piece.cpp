#include "Piece.h"
//

Piece::Piece(std::string type, unsigned int loc[], std::string color) 
{	
	_type = type; 
	_location[0] = loc[0]; 
	_location[1] = loc[1]; 
	_color = color; 
	
}
Piece::Piece(const Piece& pie)
{
	_type = pie.getType(); 
	_location[0] = pie.getLoc()[0];
	_location[1] = pie.getLoc()[1];
	_color = pie.getColor();
}

Piece::Piece()
{
}

Piece::~Piece()
{}

void Piece::setLoc(unsigned int* newLoc)
{
	_location[0] = newLoc[0];
	_location[1] = newLoc[1];
}


std::ostream& operator<<(std::ostream& os, const Piece& pie)
{
	os << pie.getType() << pie.getColor()[0] << "\t";
	return os;
}

bool Piece::isEmpty()
{
	return _type == EMPTY ? true : false;
}





