#pragma once
#include "Piece.h"
#include "Board.h"
class Pawn : public Piece
{
public:
	//Constructor
	Pawn(std::string type, unsigned int loc[], std::string color);

	//Distrcutor
	virtual ~Pawn();



	//Methods
	//bool movePiece(char** slots, unsigned int pos[]);
	virtual int canEat(Piece*** slots, unsigned int pos[]);
private:
	bool _hasMoved;
};

