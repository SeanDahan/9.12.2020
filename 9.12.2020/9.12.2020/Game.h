#pragma once
#include <vector>
#include "Board.h"
#include "Rook.h"
#include "king.h"
#include "Bishop.h"
#include "Knight.h"
#include "Queen.h"
#include "Pawn.h"

//
#define FIGURESSIZE 16

class Game
{
public:

	//Constructors
	Game();

	//Distructors
	~Game();

	//Methods



	//Getters
	Board& getBoard()  { return _brd; };
	Piece** getWFigures() { return _WFigures; };
	Piece** getBFigures() { return _BFigures; };

	//Helpers
	void fillBlack();
	void fillWhite();
	void pushAll();

	bool checkWhiteCheck();
	bool checkBlackCheck();

	//Methods
	void addToPieces(Piece* pie);

	


private:
	Board _brd;
	Piece* _WFigures[FIGURESSIZE];
	Piece* _BFigures[FIGURESSIZE];
};
