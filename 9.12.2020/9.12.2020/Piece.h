#pragma once
#include <string>
#include <iostream>
#include <vector>
#define ROOK "Rook"
#define EMPTY "Empty"
#define BLACK "Black"
#define WHITE "White"
#define KING "king"
#define BISHOP "Bishop"
#define KNIGHT "Knight"
#define QUEEN "Queen"
#define PAWN "Pawn"

//error codes
#define ONE 1
#define TWO 2
#define THREE 3
#define FOUR 4
#define FIVE 5
#define SIX 6
#define SEVEN 7
#define EIGHT 8
//
class Piece
{
public:
	//Constructors
	Piece(std::string type, unsigned int loc[], std::string color);
	Piece(const Piece& pie);
	Piece();

	//Distructor
	virtual ~Piece();

	//Getters
 	unsigned int* getLoc() const { return _location; };
	std::string getType() const { return _type; };
	std::string getColor() const { return _color; };

	//Setters
	void setLoc(unsigned int* newLoc);
	

	//Methods
	//virtual bool movePiece(char** slots, unsigned int pos[]) = 0;
	virtual int canEat(Piece*** slots, unsigned int pos[]) = 0;
	

	//Overloaders
	friend std::ostream& operator<<(std::ostream& os, const Piece& dt);

	//Helpers
	bool isEmpty();
protected:
	unsigned int* _location = new unsigned int[2];
	std::string _type;
	std::string _color;
	
};

