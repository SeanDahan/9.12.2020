#pragma once
//
#include "Piece.h"
#include "Board.h"
#include "MoveException.h"

#define BOARD_SIZE 7

class Rook : public Piece
{
public:
	//Constructor
	Rook(std::string type, unsigned int loc[], std::string color);

	//Distrcutor
	virtual ~Rook();

	

	//Methods
	//bool movePiece(char** slots, unsigned int pos[]);
	virtual int canEat(Piece*** slots, unsigned int pos[]);



};
