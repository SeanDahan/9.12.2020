#include "MoveException.h"

const char* MoveException::what()
{
    return "Move Error: The Rook can't move in diagonal";
}
