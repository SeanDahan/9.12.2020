#include "Bishop.h"

Bishop::Bishop(std::string type, unsigned int loc[], std::string color) : Piece(type, loc, color)
{
}

Bishop::~Bishop()
{
}

int Bishop::canEat(Piece*** slots, unsigned int pos[])
{
	unsigned int newLoc[2] = { _location[0], _location[1] };
	if (pos[0] >= BOARDSIZE || pos[0] < 0 || pos[1] >= BOARDSIZE || pos[1] < 0)//If move is out of the board
	{
		return FIVE;
	}
	else if (pos[0] == this->_location[0] && this->_location[1] == pos[1]) //If the move is to the same spot we are currently in
	{
		return SEVEN;
	}
	else if (slots[pos[0]][pos[1]]->getColor() == this->getColor() && slots[pos[0]][pos[1]]->getType() != EMPTY) //If there is a piece in the same color as ours in the destination spot
	{
		return THREE;
	}
	else if (abs((int)(this->_location[0] - pos[0])) != abs((int)(this->_location[1] - pos[1])))
	{
		return SIX;
	}
	else
	{
		int appliX = 1, appliY = 1;
		if (pos[0] < this->_location[0])
		{
			appliX = -1;
		}
		if (pos[1] < this->_location[1])
		{
			appliY = -1;
		}
		for (int i = 0; i < abs((int)(this->_location[0] - pos[0]))-1; i++)
		{
			newLoc[0] += appliX;
			newLoc[1] += appliY;
			//adding or substructing slots, by the move's type
			if (!slots[newLoc[0]][newLoc[1]]->isEmpty() && newLoc[0] != this->_location[0] && this->_location[1] != newLoc[1]) //Checking if there is a piece in the current spot. if yes, return false
			{
				return SIX;
			}
		}
		return 0;
	}
	return 0;
}
