#include "Knight.h"

Knight::Knight(std::string type, unsigned int loc[], std::string color) : Piece(type, loc, color)
{
}

Knight::~Knight()
{
}

int Knight::canEat(Piece*** slots, unsigned int pos[])
{
	unsigned int newLoc[2] = { _location[0], _location[1] };
	if (pos[0] >= BOARDSIZE || pos[0] < 0 || pos[1] >= BOARDSIZE || pos[1] < 0)//If move is out of the board
	{
		return FIVE;
	}
	else if (pos[0] == this->_location[0] && this->_location[1] == pos[1]) //If the move is to the same spot we are currently in
	{
		return SEVEN;
	}
	else if (slots[pos[0]][pos[1]]->getColor() == this->getColor() && slots[pos[0]][pos[1]]->getType() != EMPTY) //If there is a piece in the same color as ours in the destination spot
	{
		return THREE;
	}
	else if (abs((int)(this->_location[0] - pos[0])) == abs((int)(this->_location[1] - pos[1])))
	{
		return SIX;
	}
	else if(abs((int)(this->_location[0] - pos[0])) == 1 && abs((int)(this->_location[1] - pos[1])) == 2 || abs((int)(this->_location[0] - pos[0])) == 2 && abs((int)(this->_location[1] - pos[1])) == 1)
	{
		return 0;
	}
	else
	{
		return SIX;
	}
}
