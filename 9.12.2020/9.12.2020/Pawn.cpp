#include "Pawn.h"

Pawn::Pawn(std::string type, unsigned int loc[], std::string color) : Piece(type, loc, color)
{
	_hasMoved = false;
}

Pawn::~Pawn()
{

}

int Pawn::canEat(Piece*** slots, unsigned int pos[])
{
	unsigned int newLoc[2] = { _location[0], _location[1] };
	if (pos[0] >= BOARDSIZE || pos[0] < 0 || pos[1] >= BOARDSIZE || pos[1] < 0)//If move is out of the board
	{
		return FIVE;
	}
	else if (pos[0] == this->_location[0] && this->_location[1] == pos[1]) //If the move is to the same spot we are currently in
	{
		return SEVEN;
	}
	else if (slots[pos[0]][pos[1]]->getColor() == this->getColor() && slots[pos[0]][pos[1]]->getType() != EMPTY) //If there is a piece in the same color as ours in the destination spot
	{
		return THREE;
	}
	else if (abs((int)(pos[0] - this->_location[0])) == 2 && !_hasMoved && slots[pos[0]][pos[1]]->getType() == EMPTY)
	{
		_hasMoved = true;
		return 0;
	}
	else if (abs((int)(pos[0] - this->_location[0])) == 1 && abs((int)(pos[1] - this->_location[1])) == 0 && slots[pos[0]][pos[1]]->getType() == EMPTY) // Move
	{
		_hasMoved = true;
		return 0;
	}
	else if (abs((int)(pos[0] - this->_location[0])) == 1 && abs((int)(pos[1] - this->_location[1])) == 1 && slots[pos[0]][pos[1]]->getType() != EMPTY) // Eat
	{
		_hasMoved = true;
		return 0;
	}
	else
	{
		return SIX;
	}
}