#include "Board.h"
//
Board::Board()
{

}

Board::~Board()
{

}

void Board::printBoard()
{
	for (size_t i = 0; i < BOARDSIZE; i++)
	{
		for (size_t j = 0; j < BOARDSIZE; j++)
		{
			std::cout << *slots[i][j];
		}
		putchar('\n');
	}
}

void Board::fillEmpty()
{
	slots = (Piece***)malloc(sizeof(Piece));
	for (size_t i = 0; i < BOARDSIZE; i++)
	{
		slots[i] = (Piece**)malloc(sizeof(Piece) * BOARDSIZE);
		for (size_t j = 0; j < BOARDSIZE; j++)
		{
			slots[i][j] = (Piece*)malloc(sizeof(Piece));
		}
	}
	unsigned int loc[2] = { 0 };
	
	for (size_t i = 0; i < BOARDSIZE; i++)
	{
		loc[0] = i;
		for (size_t j = 0; j < BOARDSIZE; j++)
		{
			loc[1] = j;
			EmptyPiece* pie = new EmptyPiece(EMPTY, loc, BLACK);
			slots[i][j] = pie;
		}
	}

}

void Board::move(unsigned int* preLoc, unsigned int* newLoc)
{
}

void Board::push(Piece* pie)
{
	slots[pie->getLoc()[0]][pie->getLoc()[1]] = pie;
}

void Board::pushMove(Piece* pieces[16],unsigned int* preLoc, unsigned int* newLoc)
{
	for (size_t i = 0; i < 16; i++)
	{
		if (pieces[i]->getLoc()[0] == newLoc[0] && pieces[i]->getLoc()[1] == newLoc[1])
		{
			delete slots[newLoc[0]][newLoc[1]];
			pieces[i] = new EmptyPiece(EMPTY, preLoc, BLACK);
			break;
		}
	}
	//delete slots[newLoc[0]][newLoc[1]];
	slots[preLoc[0]][preLoc[1]]->setLoc(newLoc);
	push(slots[preLoc[0]][preLoc[1]]);
	EmptyPiece* empty = new EmptyPiece(EMPTY, preLoc, BLACK);
	push(empty);
}


