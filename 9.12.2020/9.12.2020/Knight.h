#pragma once
#include "Piece.h"
#include "Board.h"
class Knight : public Piece
{
public:
	//Constructor
	Knight(std::string type, unsigned int loc[], std::string color);

	//Distrcutor
	virtual ~Knight();



	//Methods
	//bool movePiece(char** slots, unsigned int pos[]);
	virtual int canEat(Piece*** slots, unsigned int pos[]);
};

