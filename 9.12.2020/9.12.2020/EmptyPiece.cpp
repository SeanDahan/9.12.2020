
#include "EmptyPiece.h"
//
EmptyPiece::EmptyPiece(std::string type, unsigned int loc[2], std::string color) : Piece(type,loc,color)
{

}

EmptyPiece::EmptyPiece()
{
	
}

EmptyPiece::~EmptyPiece()
{
}

bool EmptyPiece::movePiece(char** slots, unsigned int pos[])
{
	return true;
}

int EmptyPiece::canEat(Piece*** slots, unsigned int pos[])
{
	return 2;
}

