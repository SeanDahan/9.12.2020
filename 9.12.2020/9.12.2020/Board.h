#pragma once
//
#include <vector>
#include <string>
#include "EmptyPiece.h"
#include "Piece.h"
#include "king.h"
#define BOARDSIZE 8
class Board
{
public:
	//Constructor
	Board();

	//Distructor
	~Board();

	//Methods
	void printBoard();
	void fillEmpty();

	void move(unsigned int* preLoc, unsigned int* newLoc);
	void push(Piece* pie);
	void pushMove(Piece* pieces[16], unsigned int* preLoc, unsigned int* newLoc);
	

	
	//Getters
	Piece*** getPieces() { return slots; };

private:

	Piece*** slots;
};

