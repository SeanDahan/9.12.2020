#pragma once
#include "Piece.h"
#include "Board.h" 
class Queen : public Piece
{
public:
	//Constructor
	Queen(std::string type, unsigned int loc[], std::string color);

	//Distrcutor
	virtual ~Queen();



	//Methods
	//bool movePiece(char** slots, unsigned int pos[]);
	virtual int canEat(Piece*** slots, unsigned int pos[]);
};

