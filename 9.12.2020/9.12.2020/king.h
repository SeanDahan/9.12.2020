#pragma once
//
#include "Piece.h"
#include "Board.h"
#include "MoveException.h"

#define BOARD_SIZE 7

class king : public Piece
{
public:
	//Constructor
	king(std::string type, unsigned int loc[], std::string color);

	//Distrcutor
	virtual ~king();

	//Methods
	//bool movePiece(char** slots, unsigned int pos[]);
	virtual int canEat(Piece*** slots, unsigned int pos[]);
};
