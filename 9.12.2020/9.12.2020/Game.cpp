#include "Game.h"
//
Game::Game()
{	
	_brd.fillEmpty();
	fillWhite();
	fillBlack();
	pushAll();
}

Game::~Game()
{
}

void Game::fillBlack()
{
	//unsigned int loc[] = { 0 };	
	// List of Figures
	// Rook, Rook, Empty, King, Queen, Bishop, Bishop, Knight, Knight, Pawn * 8
	unsigned int loc[] = { 7,0 };
	unsigned int counter = 0, spotCounter = 0;

	for (size_t i = 0; i < FIGURESSIZE; i++)
	{
		if (i == 8)
		{
			loc[0]--;
			counter = 0;
		}

		loc[1] = counter;
		EmptyPiece* r = new EmptyPiece(EMPTY, loc, BLACK);
		_BFigures[spotCounter] = r;
		counter++;
		spotCounter++;


	}
	loc[0] = 7;
	loc[1] = 0;
	Rook* r1 = new Rook(ROOK, loc, BLACK);
	_BFigures[0] = r1;
	loc[1] = 7;
	Rook* r2 = new Rook(ROOK, loc, BLACK);
	_BFigures[1] = r2;

	//King
	
	loc[0] = 7;
	loc[1] = 3;
	king* k = new king(KING, loc, BLACK);
	_BFigures[2] = k;

	loc[1] = 4;
	Queen* q = new Queen(QUEEN, loc, BLACK);
	_BFigures[3] = q;

	loc[0] = 7;
	loc[1] = 2;
	Bishop* b1 = new Bishop(BISHOP, loc, BLACK);
	_BFigures[4] = b1;
	loc[1] = 5;
	Bishop* b2 = new Bishop(BISHOP, loc, BLACK);
	_BFigures[5] = b2;

	loc[0] = 7;
	loc[1] = 1;
	Knight* n1 = new Knight(KNIGHT, loc, BLACK);
	_BFigures[6] = n1;
	loc[1] = 6;
	Knight* n2 = new Knight(KNIGHT, loc, BLACK);
	_BFigures[7] = n2;
	//
	loc[0] = 6;
	loc[1] = 0;
	for (int i = 8; i < FIGURESSIZE; i++)
	{
		
		Pawn* p = new Pawn(PAWN, loc, BLACK);
		_BFigures[i] = p;
		++loc[1];
	}
	
}


void Game::fillWhite()
{
	// List of Figures
	// Rook, Rook, Empty, King, Queen, Bishop, Bishop, Knight, Knight, Pawn * 8

	unsigned int loc[] = { 0,0 };
	unsigned int counter = 0, spotCounter = 0;

	for (size_t i = 0; i < FIGURESSIZE; i++)
	{
		if (i == 8)
		{
			loc[0]++;
			counter = 0;
		}

		loc[1] = counter;
		EmptyPiece* r = new EmptyPiece(EMPTY, loc, BLACK);
		_WFigures[spotCounter] = r;
		counter++;
		spotCounter++;


	}

	//unsigned int loc[] = { 0,0 };
	loc[0] = 0;
	loc[1] = 0;
	Rook* r1 = new Rook(ROOK, loc, WHITE);
	_WFigures[0] = r1;
	loc[1] = 7;
	Rook* r2 = new Rook(ROOK, loc,WHITE);
	_WFigures[1] = r2;

	//king
	loc[0] = 0;
	loc[1] = 3;
	king* k = new king(KING, loc, WHITE);
	_WFigures[2] = k;

	loc[1] = 4;
	Queen* q = new Queen(QUEEN, loc, WHITE);
	_WFigures[3] = q;
	loc[0] = 0;
	loc[1] = 2;
	Bishop* b1 = new Bishop(BISHOP, loc, WHITE);
	_WFigures[4] = b1;
	loc[1] = 5;
	Bishop* b2 = new Bishop(BISHOP, loc, WHITE);
	_WFigures[5] = b2;

	loc[0] = 0;
	loc[1] = 1;
	Knight* n1 = new Knight(KNIGHT, loc, WHITE);
	_WFigures[6] = n1;
	loc[1] = 6;
	Knight* n2 = new Knight(KNIGHT, loc, WHITE);
	_WFigures[7] = n2;
	loc[0] = 1;
	loc[1] = 0;
	for (int i = 8; i < FIGURESSIZE; i++)
	{
		
		Pawn* p = new Pawn(PAWN, loc, WHITE);
		_WFigures[i] = p;
		++loc[1];
	}


}

void Game::pushAll()
{
	/*
	_brd.push(_WFigures[0]);
	_brd.push(_WFigures[1]);
	_brd.push(_WFigures[2]);
	_brd.push(_WFigures[3]);
	_brd.push(_WFigures[4]);
	_brd.push(_WFigures[5]);
	_brd.push(_WFigures[6]);
	_brd.push(_WFigures[7]);

	_brd.push(_BFigures[0]);
	_brd.push(_BFigures[1]);
	_brd.push(_BFigures[2]);
	_brd.push(_BFigures[3]);
	_brd.push(_BFigures[4]);
	_brd.push(_BFigures[5]);
	_brd.push(_BFigures[6]);
	_brd.push(_BFigures[7]);
	*/
	// Pushing White side
	for (Piece* val : _WFigures)
	{
		_brd.push(val);
	}

	// Pushing Black side
	for (Piece* val : _BFigures)
	{
		_brd.push(val);
	}

	
}

bool Game::checkWhiteCheck()
{
	for (size_t i = 0; i < FIGURESSIZE; i++)
	{
		if (_WFigures[i]->canEat(_brd.getPieces(), _BFigures[2]->getLoc()) == 0)
		{
			return true;
		}
	}
	return false;
}

bool Game::checkBlackCheck()
{
	for (size_t i = 0; i < FIGURESSIZE; i++)
	{
		if (_BFigures[i]->canEat(_brd.getPieces(), _WFigures[2]->getLoc()) == 0)
		{
			return true;
		}
		
	}
	return false;
}

void Game::addToPieces(Piece* pie)
{
	if (pie->getColor() == WHITE)
	{
		int i = 0;
		for (auto val : _WFigures)
		{
			if (val->getType() == EMPTY)
			{
				_WFigures[i] = pie;
				return;
			}
			else
			{
				++i;
			}
		}
	}
	else
	{
		int i = 0;
		for (auto val : _BFigures)
		{
			if (val->getType() == EMPTY)
			{
				_BFigures[i] = pie;
				return;
			}
			else
			{
				++i;
			}
		}
	}
}


