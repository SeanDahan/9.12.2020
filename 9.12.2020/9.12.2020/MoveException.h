#pragma once
#include <exception>
class MoveException : std::exception
{
public:
	virtual const char* what();
};