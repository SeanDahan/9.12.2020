/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Game.h"
#include <windows.h>
using std::cout;
using std::endl;
using std::string;

unsigned int* ConvertStringToXY(std::string str)
{
	unsigned int* retu = new unsigned int[2];
	retu[0] = (str[1] - 49);
	retu[1] = str[0] - 97;
	return retu;
}

void main()
{
	srand(time_t(NULL));

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	LPCSTR  a = (LPCSTR)"..\\chessGraphics11\\chessGraphics\\bin\\Debug\\chessGraphics.exe";

	if (!CreateProcess(a, NULL, NULL, NULL, 0, 0, NULL, NULL, &si, &pi))
	{
		printf("Error launching program! Check your directory and locations.");
		//free(buffer);
		return;
	}
	//system("..\\chessGraphics11\\chessGraphics\\bin\\Debug\\chessGraphics.exe");//D:\Magshimim\Year 2\C++\9.12.2020\9.12.2020\chessGraphics\bin\Debug
	Sleep(1000);
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"); // just example...
	//strcpy_s(msgToGraphics, "rnbkqbnr################################################RNBKQBNR1"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = "a";
	int currentPlayer = 0;//1 Black, 0 White
	std::string players[2] = { WHITE,BLACK };
	char code = 'a';
	char codeStr[2] = { 0 };

	unsigned int tempLoc[2] = { 0 };
	std::string tempType = "";
	std::string tempColor = "";

	Game gme;
	Piece* tempDest;
	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		
		// YOUR CODE
		msgFromGraphics = p.getMessageFromGraphics();
		if (msgFromGraphics != "quit")
		{
			if (gme.getBoard().getPieces()[ConvertStringToXY(msgFromGraphics.substr(0, 2))[0]][ConvertStringToXY(msgFromGraphics.substr(0, 2))[1]]->getColor() == players[currentPlayer])
			{
				code = gme.getBoard().getPieces()[ConvertStringToXY(msgFromGraphics.substr(0, 2))[0]][ConvertStringToXY(msgFromGraphics.substr(0, 2))[1]]->canEat(gme.getBoard().getPieces(), ConvertStringToXY(msgFromGraphics.substr(2, 2))) + 48;
				//tempDest = new Piece(gme.getBoard().getPieces()[ConvertStringToXY(msgFromGraphics.substr(2, 2))[0]][ConvertStringToXY(msgFromGraphics.substr(2, 2))[1]]);
				tempDest = gme.getBoard().getPieces()[ConvertStringToXY(msgFromGraphics.substr(2, 2))[0]][ConvertStringToXY(msgFromGraphics.substr(2, 2))[1]];
				tempLoc[0] = tempDest->getLoc()[0];
				tempLoc[1] = tempDest->getLoc()[1];
				tempColor = tempDest->getColor();
				tempType = tempDest->getType();
				std::cout << "Code is " << code << std::endl;
				if (code == '0' || code == '1')
				{
					gme.getBoard().pushMove(currentPlayer == 1 ? gme.getWFigures() : gme.getBFigures(), ConvertStringToXY(msgFromGraphics.substr(0, 2)), ConvertStringToXY(msgFromGraphics.substr(2, 2)));
					if (currentPlayer == 1 ? gme.checkBlackCheck() : gme.checkWhiteCheck())
						code = '1';


					else if (currentPlayer == 1 ? gme.checkWhiteCheck() : gme.checkBlackCheck())
					{
						code = '4';
						gme.getBoard().pushMove(currentPlayer == 1 ? gme.getWFigures() : gme.getBFigures(), ConvertStringToXY(msgFromGraphics.substr(2, 2)), ConvertStringToXY(msgFromGraphics.substr(0, 2)));
						if (tempType == "Pawn")
							tempDest = new Pawn(tempType, tempLoc, tempColor);
						else if (tempType == "Bishop")
							tempDest = new Bishop(tempType, tempLoc, tempColor);
						else if (tempType == "Rook")
							tempDest = new Rook(tempType, tempLoc, tempColor);
						else if (tempType == "king")
							tempDest = new king(tempType, tempLoc, tempColor);
						else if (tempType == "Knight")
							tempDest = new Knight(tempType, tempLoc, tempColor);
						else if (tempType == "Queen")
							tempDest = new Queen(tempType, tempLoc, tempColor);
						gme.getBoard().push(tempDest);
						gme.addToPieces(tempDest);
						currentPlayer == 1 ? currentPlayer = 0 : currentPlayer = 1;
					}
					currentPlayer == 1 ? currentPlayer = 0 : currentPlayer = 1;

				}

			}
			else
			{
				std::cout << "Current piece's color is " << gme.getBoard().getPieces()[ConvertStringToXY(msgFromGraphics.substr(0, 2))[0]][ConvertStringToXY(msgFromGraphics.substr(0, 2))[1]]->getColor() << std::endl;
				code = '2';
			}
			codeStr[0] = code;
			codeStr[1] = '\0';
			strcpy_s(msgToGraphics, codeStr); // msgToGraphics should contain the result of the operation

			/******* JUST FOR EREZ DEBUGGING ******//*
			int r = rand() % 10; // just for debugging......
			msgToGraphics[0] = (char)(1 + '0');
			msgToGraphics[1] = 0;
			/******* JUST FOR EREZ DEBUGGING ******/

			//Comment
			// return result to graphics		
			p.sendMessageToGraphics(msgToGraphics);

			// get message from graphics
			std::cout << std::endl << "Current player is " << currentPlayer;
		}
		
		
	}

	for (size_t i = 0; i < BOARDSIZE; i++)
	{
		for (size_t j = 0; j < BOARDSIZE; j++)
		{
			delete gme.getBoard().getPieces()[i][j];
		}
	}
	delete[] gme.getBoard().getPieces();
	
	p.close();
}